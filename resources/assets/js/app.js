var pais = new Vue({
    el: '#pais',
    created: function(){
        this.getPaises();
    },
    data: {
        paises: [],
        codigo: '',
        nombre: '',
        errores: [],
        fillEmpresa: {'id': '', 'codigo':'', 'nombre':'}
    },
    methods: {
        getPaises: function() {
            var urlPaises = 'paises';
            axios.get(urlPaises).then(response => {
                this.paises = response.data
            });
        },
        editarPais: function(pais) {
            this.fillPais.id = pais.id;
            this.fillPais.codigo = pais.codigo;
            this.fillPais.nombre = pais.nombre;
            $('#pais_editar').modal('show');
        },
        updatePais: function(id) {
            var url = 'paises/' + id;
            axios.put(url, this.fillPais).then(response => {
                this.getPaises();
                this.fillPais = {'id': '', 'codigo':'', 'nombre':''};
                this.errors = [];
                $('#pais_editar').modal('hide');
            }).catch(error => {
                this.errors = error.response.data
            });
        }
    }
});
