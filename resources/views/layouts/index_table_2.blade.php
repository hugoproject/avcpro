@extends('layouts.main')
@section('content')
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">

        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        @yield('title')
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        @yield('create_new')
                    </li>
                </ul>
            </div>
        </div>

        <!--begin:: Widgets/Application Sales-->
        <div class="m-portlet__body">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                <!--begin::Thead-->
                <thead>
                    <tr>
                        @yield('columns_name')
                    </tr>
                </thead>
                <!--end::Thead-->
                <!--begin::Tbody-->
                <tbody>
                    @yield('body_table')
                </tbody>
                <!--end::Tbody-->
            </table>
            <!--end::Table-->
        </div>
        <!--end:: Widgets/Application Sales-->
        </div>
    </div>
@stop
