@extends('layouts.main')
@section('content')
@include('layouts._error')
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        @yield('titulo_form')
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="{{ route('home')}}" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="{{ route($index_path) }}" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    @yield('subtitulo_form')
                                </span>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    @yield('tag_path')
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div>
            <div class="m-portlet">
                <div class="tab-content">
                    <div class="tab-pane active" id="m_user_profile_tab_1">
                        @yield('init_form')
                            {{ csrf_field() }}
                                <div class="m-portlet__body">
                                    @yield('content_form')
                                    <div class="m-portlet__foot m-portlet__foot--fit">
                                        <div class="m-form__actions">
                                            <div class="row">
                                                <div class="col-2"></div>
                                                <div class="col-7">
                                                    <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                        Guardar cambios
                                                    </button>
                                                    &nbsp;&nbsp;
                                                    <a href="{{ route($index_path) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                        Cancelar
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
