<!-- Se agrega como vista parcial indicando el quion bajo al inicio del nombre-->
@if(count($errors))
<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
    <div class="m-alert__icon">
        <i class="flaticon-danger"></i>
        <span></span>
    </div>
    <div class="m-alert__text">
        @foreach($errors->all() as $error)
            <li>
                <strong>{{ $error }}!</strong>
            </li>
        @endforeach
    </div>  
    <div class="m-alert__close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        </button>
    </div>
</div>
@endif
