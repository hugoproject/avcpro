@extends('layouts.form_main_crud')
@section('titulo_form')
    @if($entidadComercial->exists)
        Editar entidad comercial
    @else
        Crear nueva entidad
    @endif
@endsection
@section('subtitulo_form')
    Entidades comerciales
@endsection
@section('tag_path')
    <span class="m--font-info">
        @if($entidadComercial->exists)
            Editar
        @else
            Crear nuevo
        @endif
    </span>
@endsection
@section('init_form')
    @if($entidadComercial->exists)
        <form action= "{{ route('update_entidad_comercial_path', ['entidadComercial'=>$entidadComercial->id])}}" method="POST" class="m-form m-form--fit m-form--label-align-right">
        {{ method_field('PUT')}}
    @else
        <form action= "{{ route('store_entidad_comercial_path')}}" method="POST" class="m-form m-form--fit m-form--label-align-right">
    @endif
@endsection
@section('content_form')
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Nombre
      </label>
      <div class="col-7">
          <input type="text" name="nombre" class="form-control m-input" value="{{$entidadComercial->nombre}}">
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      NIT
      </label>
      <div class="col-7">
          <input type="text" name="nit" class="form-control m-input" value="{{ $entidadComercial->nit}}">
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Dirección
      </label>
      <div class="col-7">
          <input type="text" name="direccion" class="form-control m-input" value="{{ $entidadComercial->direccion}}">
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Teléfono
      </label>
      <div class="col-7">
          <input type="text" name="telefono" class="form-control m-input" value="{{ $entidadComercial->telefono}}">
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Correo electrónico
      </label>
      <div class="col-7 input-group">
          <div class="input-group-prepend"><span class="input-group-text" id="basic-addon1">@</span></div>
          <input type="text" name="email" class="form-control m-input" value="{{ $entidadComercial->email}}">
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      País
      </label>
      <div class="col-7">
        @if($entidadComercial->exists)
          {!! Form::select('paisId', $paises, $pais, ['class' => 'form-control select2']) !!}
        @else
          {!! Form::select('paisId', $paises, null, ['class' => 'form-control select2']) !!}
        @endif
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Categoría
      </label>
      <div class="col-7">
        @if($entidadComercial->exists)
          {!! Form::select('categorias[]', $categorias, $selectCategorias, ['multiple'=>'multiple', 'class' => 'form-control select2']) !!}
        @else
          {!! Form::select('categorias[]', $categorias, null, ['multiple'=>'multiple', 'class' => 'form-control select2']) !!}
        @endif
      </div>
  </div>
@endsection