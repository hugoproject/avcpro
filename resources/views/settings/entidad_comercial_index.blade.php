@extends('layouts.index_table')
@section('title')
        Entidades comerciales
@stop
@section('create_new')
    <a href="{{ route('create_entidad_comercial_path') }}" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
        <span>
            <i class="la la-plus"></i>
            <span>
                Crear nuevo
            </span>
        </span>
    </a>
@stop
@section('columns_name')
    <td class="m-widget11__app">
        #
    </td>
    <td class="m-widget11__app">
        Nombre
    </td>
    <td class="m-widget11__sales">
        NIT
    </td>
    <td class="m-widget11__sales">
        Dirección
    </td>
    <td class="m-widget11__sales">
        Teléfono
    </td>
    <td class="m-widget11__sales">
        Acciones
    </td>
@stop
@section('body_table')
    @foreach($entidadesComerciales as $entidad)
        <tr>
            <td>
                <span class="m-widget11__title">
                    {{$entidad->id}}
                </span>
            </td>
            <td><span class="m-widget11__title">{{$entidad->nombre}}</span></td>
            <td><span class="m-widget11__title">{{$entidad->nit}}</span></td>
            <td><span class="m-widget11__title">{{$entidad->direccion}}</span></td>
            <td><span class="m-widget11__title">{{$entidad->telefono}}</span></td>
            <td nowrap="">
                <span class="dropdown">
                    <a href="{{ route('edit_entidad_comercial_path', $entidad->id) }}" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
                      <i class="la la-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(32px, 27px, 0px);">
                        <form action="{{ route('destroy_entidad_comercial_path',$entidad->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </form>
                    </div>
                </span>
                <a href="{{ route('edit_entidad_comercial_path', $entidad->id) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                  <i class="la la-edit"></i>
                </a>
            </td>
        </tr>
    @endforeach
@stop