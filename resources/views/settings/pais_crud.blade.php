@extends('layouts.form_main_crud')
@section('titulo_form')
    @if($pais->exists)
        Editar país
    @else
        Crear nuevo país
    @endif
@endsection
@section('subtitulo_form')
    Lista de paises
@endsection
@section('tag_path')
    <span class="m--font-info">
        @if($pais->exists)
            Editar
        @else
            Crear nuevo
        @endif
    </span>
@endsection
@section('init_form')
    @if($pais->exists)
        <form action= "{{ route('update_pais_path', ['pais'=>$pais->id])}}" method="POST" class="m-form m-form--fit m-form--label-align-right">
        {{ method_field('PUT')}}
    @else
        <form action= "{{ route('store_pais_path')}}" method="POST" class="m-form m-form--fit m-form--label-align-right">
    @endif
@endsection
@section('content_form')
    <div class="form-group m-form__group row">
       <label for="example-text-input" class="col-2 col-form-label">
       Código
       </label>
       <div class="col-7">
          <input type="text" name="codigo" class="form-control m-input" value="{{$pais->codigo}}">
       </div>
    </div>
    <div class="form-group m-form__group row">
       <label for="example-text-input" class="col-2 col-form-label">
       Nombre
       </label>
       <div class="col-7">
          <input type="text" name="nombre" class="form-control m-input" value="{{$pais->nombre}}">
       </div>
    </div>
@endsection