
<form method="POST" v-on:submit.prevent="mostrarEmpresas" class="m-form m-form--fit m-form--label-align-right">
    <div class="modal-mask" id="seleccionar_empresa">
        <div class='modal-dialog'>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Seleccionar empresa
                    </h5>
                </div>
                <div class="modal-body">
                    <span v-for="error in errors" class="text-danger">@{{ error }}</span>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Guardar">
                </div>
            </div>
        </div>
    </div>
</form>

