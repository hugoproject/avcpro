@extends('layouts.app')
@section('content')
<div id="pais" class="col-xl-12">
    <!--begin:: Widgets/Application Sales-->
    <div class="m-portlet m-portlet--full-height ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Lista de paises
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="tab-content">
                <div class="tab-pane active" id="m_widget11_tab1_content">
                    <!--begin::Widget 11-->
                    <div class="m-widget11">
                        <div class="table-responsive">
                            <!--begin::Table-->
                            <table class="table">
                                <!--begin::Thead-->
                                <thead>
                                    <tr>
                                        <td class="m-widget11__label">
                                            #
                                        </td>
                                        <td class="m-widget11__app">
                                            Código
                                        </td>
                                        <td class="m-widget11__sales">
                                            Nombre
                                        </td>
                                        <td class="m-widget11__sales">
                                            Acciones
                                        </td>
                                    </tr>
                                </thead>
                                <!--end::Thead-->
                                <!--begin::Tbody-->
                                <tbody>
                                        <tr v-for="pais in paises">
                                            <td>
                                                <span class="m-widget11__title">
                                                    @{{pais.id}}
                                                </span>
                                            </td>
                                            <td><span class="m-widget11__title">@{{pais.codigo}}</span></td>
                                            <td><span class="m-widget11__title">@{{pais.nombre}}</span></td>
                                            <td nowrap="">
                                                <a href="#" v-on:click.prevent="editarPais(pais)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">
                                                    <i class="la la-edit"></i>
                                                </a>
                                            </td>
                                        </tr>
                                </tbody>
                                <!--end::Tbody-->
                            </table>
                            @include('configuraciones.pais_edit')
                            <!--end::Table-->
                        </div>
                    </div>
                    <!--end::Widget 11-->
                </div>
            </div>
        </div>
    </div>
    <!--end:: Widgets/Application Sales-->
</div>
@endsection
