@extends('layouts.form_main_crud')
@section('titulo_form')
    @if($empresa->exists)
        Editar empresa
    @else
        Crear nueva empresa
    @endif
@endsection
@section('subtitulo_form')
    Lista de empresas
@endsection
@section('tag_path')
    <span class="m--font-info">
        @if($empresa->exists)
            Editar
        @else
            Crear nuevo
        @endif
    </span>
@endsection
@section('init_form')
    @if($empresa->exists)
        <form action= "{{ route('update_empresa_path', ['empresa'=>$empresa->id])}}" method="POST" class="m-form m-form--fit m-form--label-align-right">
        {{ method_field('PUT')}}
    @else
        <form action= "{{ route('store_empresa_path')}}" method="POST" class="m-form m-form--fit m-form--label-align-right">
    @endif
@endsection
@section('content_form')
  <div class="form-group m-form__group row">
      <div class="col-10 ml-auto">
          <h3 class="m-form__section">
              1. Detalles de la empresa
          </h3>
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Razon social
      </label>
      <div class="col-7">
          <input type="text" name="razonsocial" class="form-control m-input" value="{{$empresa->razonsocial}}">
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Nombre comercial
      </label>
      <div class="col-7">
          <input type="text" name="nombreComercial" class="form-control m-input" value="{{ $empresa->nombreComercial}}">
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      NIT
      </label>
      <div class="col-7">
          <input type="text" name="nit" class="form-control m-input value="{{ $empresa->nit}}">
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Teléfono No.
      </label>
      <div class="col-7 input-group">
          <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
          <input type="text" name="telefono" class="form-control m-input" value="{{ $empresa->telefono}}">
      </div>
  </div>
  <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
  <div class="form-group m-form__group row">
      <div class="col-10 ml-auto">
          <h3 class="m-form__section">
              2. Dirección
          </h3>
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Dirección
      </label>
      <div class="col-7">
          <input type="text" name="direccion" class="form-control m-input" value="{{ $empresa->direccion}}">
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      País
      </label>
      <div class="col-7">
        @if ($empresa->exists)
          {!! Form::select('paisId', $paises, $pais, ['class' => 'form-control select2']) !!}
        @else
          {!! Form::select('paisId', $paises, null, ['class' => 'form-control select2']) !!}
        @endif
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Ciudad
      </label>
      <div class="col-7">
          <input name="ciudad" class="form-control m-input" type="text" value="{{ $empresa->ciudad }}">
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Código postal
      </label>
      <div class="col-7">
          <input name="codigoPostal" class="form-control m-input" type="text" value="{{ $empresa->codigoPostal}}">
      </div>
  </div>
  <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
  <div class="form-group m-form__group row">
      <div class="col-10 ml-auto">
          <h3 class="m-form__section">
              3. Sitios web
          </h3>
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Correo electrónico
      </label>
      <div class="col-7 input-group">
          <span class="input-group-text" id="basic-addon1">@</span>
          <input type="text" name="email" class="form-control m-input" value="{{$empresa->email}}">
      </div>
  </div>
  <div class="form-group m-form__group row">
      <label for="example-text-input" class="col-2 col-form-label">
      Dirección
      </label>
      <div class="col-7">
          <input name="sitioWeb" class="form-control m-input" type="text" placeholder="https://www.ejemplo.com" value="{{ $empresa->sitioWeb}}">
      </div>
  </div>
@endsection