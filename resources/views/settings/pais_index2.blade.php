@extends('layouts.index_table_2')
@section('title')
        Lista de paises
@stop
@section('create_new')
    <a href="{{ route('create_pais_path') }}" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
        <span>
            <i class="la la-plus"></i>
            <span>
                Crear nuevo país
            </span>
        </span>
    </a>
@stop
@section('columns_name')
    <th> # </th>
    <th> Código </th>
    <th> Nombre </th>
    <th> Acciones </th>
@stop
@section('body_table')
    @foreach($paises as $pais)
        <tr>
            <td> {{$pais->id}} </td>
            <td> {{$pais->codigo}} </td>
            <td> {{$pais->nombre}} </td>
            <td nowrap="">
                <span class="dropdown">
                    <a href="{{ route('edit_pais_path', $pais->id) }}" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
                      <i class="la la-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(32px, 27px, 0px);">
                        <form action="{{ route('destroy_pais_path',$pais->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </form>
                    </div>
                </span>
                <a href="{{ route('edit_pais_path', $pais->id) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                  <i class="la la-edit"></i>
                </a>
            </td>
        </tr>
    @endforeach
@stop
