@extends('layouts.main')
@section('content')
<div class="col-xl-12">
    <!--begin:: Widgets/Application Sales-->
    <div class="m-portlet m-portlet--full-height ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Lista de empresas
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('create_empresa_path') }}" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-plus"></i>
                                <span>
                                    Crear nuevo
                                </span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="tab-content">
                <div class="tab-pane active" id="m_widget11_tab1_content">
                    <!--begin::Widget 11-->
                    <div class="m-widget11">
                        <div class="table-responsive">
                            <!--begin::Table-->
                            <table class="table">
                                <!--begin::Thead-->
                                <thead>
                                    <tr>
                                        <td class="m-widget11__label">
                                            #
                                        </td>
                                        <td class="m-widget11__app">
                                            Razonsocial
                                        </td>
                                        <td class="m-widget11__sales">
                                            Nombre comercial
                                        </td>
                                        <td class="m-widget11__sales">
                                            NIT
                                        </td>
                                        <td class="m-widget11__sales">
                                            Dirección
                                        </td>
                                        <td class="m-widget11__sales">
                                            Teléfono
                                        </td>
                                        <td class="m-widget11__sales">
                                            Acciones
                                        </td>
                                    </tr>
                                </thead>
                                <!--end::Thead-->
                                <!--begin::Tbody-->
                                <tbody>
                                    @foreach($empresas as $empresa)
                                    <tr>
                                        <td>
                                            <span class="m-widget11__title">
                                                {{$empresa->id}}
                                            </span>
                                        </td>
                                        <td><span class="m-widget11__title">{{$empresa->razonsocial}}</span></td>
                                        <td><span class="m-widget11__title">{{$empresa->nombreComercial}}</span></td>
                                        <td><span class="m-widget11__title">{{$empresa->nit}}</span></td>
                                        <td><span class="m-widget11__title">{{$empresa->direccion}}</span></td>
                                        <td><span class="m-widget11__title">{{$empresa->telefono}}</span></td>
                                        <td nowrap="">
                                            <span class="dropdown">
                                                <a href="{{ route('edit_empresa_path', $empresa->id) }}" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
                                                  <i class="la la-ellipsis-h"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(32px, 27px, 0px);">
                                                    <form action="{{ route('destroy_empresa_path',$empresa->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Eliminar</button>
                                                    </form>
                                                </div>
                                            </span>
                                            <a href="{{ route('edit_empresa_path', $empresa->id) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                                              <i class="la la-edit"></i>
                                            </a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <!--end::Tbody-->
                            </table>
                            <!--end::Table-->
                        </div>
                    </div>
                    <!--end::Widget 11-->
                </div>
            </div>
        </div>
    </div>
    <!--end:: Widgets/Application Sales-->
</div>
@endsection
