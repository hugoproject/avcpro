<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Empresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razonsocial');
            $table->string('nombreComercial');
            $table->string('nit');
            $table->string('direccion');
            $table->string('ciudad');
            $table->string('telefono');
            $table->string('email');
            $table->string('codigoPostal')->nullable();
            $table->string('sitioWeb')->nullable();
            $table->timestamps();
            $table->unsignedInteger('paisId');
            $table->foreign('paisId')->references('id')->on('paises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
