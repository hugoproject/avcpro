<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group([
    'middleware' => 'auth', 'middleware' => 'checkempresa'
    ], function() {
    Route::get('/settings', 'SettingsController@index')->name('settings_index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/empresas', 'EmpresaController@index')->name('index_empresa_path');
    Route::get('/empresas/crear', 'EmpresaController@create')->name('create_empresa_path');
    Route::post('/empresas', 'EmpresaController@store')->name('store_empresa_path');
    Route::get('/empresas/{empresa}/edit', 'EmpresaController@edit')->name('edit_empresa_path');
    Route::put('/empresas/{empresa}', 'EmpresaController@update')->name('update_empresa_path');
    Route::delete('/empresas/{empresa}', 'EmpresaController@destroy')->name('destroy_empresa_path');
    Route::get('/paises', 'PaisController@index')->name('index_pais_path');
    Route::get('/paises/crear', 'PaisController@create')->name('create_pais_path');
    Route::post('/paises', 'PaisController@store')->name('store_pais_path');
    Route::get('/paises/{pais}/edit', 'PaisController@edit')->name('edit_pais_path');
    Route::put('/paises/{pais}', 'PaisController@update')->name('update_pais_path');
    Route::delete('/paises/{pais}', 'PaisController@destroy')->name('destroy_pais_path');
    Route::get('/entidades_comerciales', 'EntidadComercialController@index')->name('index_entidad_comercial_path');
    Route::get('/entidades_comerciales/crear', 'EntidadComercialController@create')->name('create_entidad_comercial_path');
    Route::post('/entidades_comerciales', 'EntidadComercialController@store')->name('store_entidad_comercial_path');
    Route::get('/entidades_comerciales/{entidad_comercial}/edit', 'EntidadComercialController@edit')->name('edit_entidad_comercial_path');
    Route::put('/entidades_comerciales/{entidad_comercial}', 'EntidadComercialController@update')->name('update_entidad_comercial_path');
    Route::delete('/entidades_comerciales/{entidad_comercial}', 'EntidadComercialController@destroy')->name('destroy_entidad_comercial_path');
    // Route::put('/paises/{pais}', 'PaisController@update')->name('update_pais_path');
    // Route::resource('/paises', 'PaisController', ['only' => 'index', 'update','edit']);
});
Route::get('/', function () {
    return view('welcome');
});
