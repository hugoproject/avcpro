<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EntidadComercial extends Model
{
    protected $table = 'entidades_comerciales';
    protected $fillable = ['id', 'nombre', 'nit', 'telefono', 'direccion', 'email','paisId','categorias'];
}
