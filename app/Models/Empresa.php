<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresas';
    protected $fillable = ['id','razonsocial', 'nombreComercial', 'nit', 'direccion', 'telefono', 'paisId', 'codigoPostal', 'sitioWeb', 'email'];
}
