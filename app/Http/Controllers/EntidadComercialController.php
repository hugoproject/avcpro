<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EntidadComercial;
use App\Models\Pais;
use App\Models\Categoria;

class EntidadComercialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lista = EntidadComercial::all();

        return view('settings.entidad_comercial_index')->with(['entidadesComerciales'=>$lista]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $entidadComercial = new EntidadComercial();
        $paises = Pais::get()->pluck('nombre','id');
        $categorias = Categoria::get()->pluck('nombre','id');
        $index_path = 'index_entidad_comercial_path';

        return view(
            'settings.entidad_comercial_crud', 
            compact('entidadComercial'))->with([
                'index_path'=>$index_path,
                'paises'=>$paises,
                'categorias'=>$categorias
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entidadComercial = new EntidadComercial();
        $entidadComercial->nombre = $request->get('nombre');
        $entidadComercial->nit = $request->get('nit');
        $entidadComercial->direccion = $request->get('direccion');
        $entidadComercial->telefono = $request->get('telefono');
        $entidadComercial->email = $request->get('email');
        $entidadComercial->categorias = $request->get('categorias');
        $entidadComercial->paisId = $request->get('paisId');

        $categorias = $request->input('categorias');
        $entidadComercial->categorias = implode(',',$categorias);
        $entidadComercial->save();

        return redirect()->route('index_entidad_comercial_path');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entidadComercial = EntidadComercial::find($id);
        $categorias = Categoria::get()->pluck('nombre','id');
        $paises = Pais::get()->pluck('nombre','id');
        $selectCategorias = explode(',', $entidadComercial->categorias);
        $index_path = 'index_entidad_comercial_path';
        $pais = $entidadComercial->paisId;

        return view(
            'settings.entidad_comercial_crud', 
            compact('entidadComercial'))->with([
                'index_path'=>$index_path,
                'paises'=>$paises,
                'pais' => $pais,
                'categorias'=>$categorias,
                'selectCategorias'=>$selectCategorias
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EntidadComercial $entidadComercial)
    {
        $entidadComercial->nombre = $request->get('nombre');
        $entidadComercial->nit = $request->get('nit');
        $entidadComercial->direccion = $request->get('direccion');
        $entidadComercial->telefono = $request->get('telefono');
        $entidadComercial->email = $request->get('email');
        $entidadComercial->categorias = $request->get('categorias');
        $entidadComercial->paisId = $request->get('paisId');

        $categorias = $request->input('categorias');
        $entidadComercial->categorias = implode(',',$categorias);
        $entidadComercial->save();

        return redirect()->route('index_entidad_comercial_path');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EntidadComercial $entidadComercial)
    {
        $entidadComercial->delete();
        return redirect()->route('index_entidad_comercial_path');
    }
}
