<?php

namespace App\Http\Controllers;

use App\Models\Pais;
use Illuminate\Http\Request;
use App\Http\Requests\PaisRequest;

class PaisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lista = Pais::all();

        return view(
            'settings.pais_index')->with(['paises'=>$lista]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pais = new Pais();
        $index_path = 'index_pais_path';
        return view(
            'settings.pais_crud',
            compact('pais'))->with(
                ['index_path'=>$index_path]
        );
    }

        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaisRequest $request)
    {
        $pais = new Pais();

        $pais->codigo = $request->get('codigo');
        $pais->nombre = $request->get('nombre');
        $pais->save();

        return redirect()->route('index_pais_path');
    }

        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pais = Pais::find($id);
        $paises = Pais::all();
        $index_path = 'index_pais_path';
        return view(
            'settings.pais_crud', 
            compact('paises'))->with(['pais'=>$pais, 'index_path'=>$index_path]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function update(PaisRequest $request, Pais  $pais)
    {
        $pais->codigo = $request->get('codigo');
        $pais->nombre = $request->get('nombre');
        $pais->save();

        return redirect()->route('index_pais_path');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pais $pais)
    {
        $pais->delete();
        return redirect()->route('index_pais_path');
    }
}
