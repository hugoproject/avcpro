<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Empresa;
use App\Http\Requests\EmpresaRequest;
use App\Models\Pais;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lista = Empresa::all();

        return view('settings.empresa_index')->with(['empresas'=>$lista]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresa = new Empresa();
        $paises = Pais::get()->pluck('nombre','id');
        $index_path = 'index_empresa_path';

        return view(
            'settings.empresa_crud',
            compact('empresa'))->with([
                'paises'=>$paises,
                'index_path'=>$index_path
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmpresaRequest $request)
    {
        $empresa = new Empresa();

        $empresa->razonsocial = $request->get('razonsocial');
        $empresa->nombreComercial = $request->get('nombreComercial');
        $empresa->nit = $request->get('nit');
        $empresa->direccion = $request->get('direccion');
        $empresa->telefono = $request->get('telefono');
        $empresa->paisId = $request->get('paisId');
        $empresa->ciudad = $request->get('ciudad');
        $empresa->email = $request->get('email');
        $empresa->codigoPostal = $request->get('codigoPostal');
        $empresa->sitioWeb = $request->get('sitioWeb');
        $empresa->save();

        return redirect()->route('index_empresa_path');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Empresa $empresa)
    {
        $paises = Pais::pluck('nombre','id');
        $pais = $empresa->paisId;
        $index_path = 'index_empresa_path';

        return view(
            'settings.empresa_crud', 
            compact('empresa'))->with([
                'pais'=>$pais, 
                'paises'=>$paises,
                'index_path'=>$index_path
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmpresaRequest $request, $id)
    {
        $empresa = Empresa::find($id);
        $empresa->razonsocial = $request->get('razonsocial');
        $empresa->nombreComercial = $request->get('nombreComercial');
        $empresa->nit = $request->get('nit');
        $empresa->direccion = $request->get('direccion');
        $empresa->telefono = $request->get('telefono');
        $empresa->paisId = $request->get('paisId');
        $empresa->ciudad = $request->get('ciudad');
        $empresa->email = $request->get('email');
        $empresa->codigoPostal = $request->get('codigoPostal');
        $empresa->sitioWeb = $request->get('sitioWeb');
        $empresa->save();

        return redirect()->route('index_empresa_path');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empresa $empresa)
    {
        $empresa->delete();
        return redirect()->route('index_empresa_path');
    }
}
